echo '##################'
echo '# APK GENERATION #'
echo '##################'

echo ""
echo "removing platforms/android/"
rm -rf platforms/android/

echo ""
echo "removing platforms/android/"
rm ci/echecsetmoi.apk

echo ""
echo "add android platform"
cordova platform add android

echo ""
echo "building app"
npm run --prefix ../client/ cordova-build && cordova build android --release
echo ""
echo "jarsigning"
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -storepass echecsetmoi34 -keystore ci/echecsetmoi.keystore platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk echecsetmoi

echo ""
echo "zipaligning"
$ANDROID_HOME/build-tools/28.0.2/zipalign.exe -v 4 platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk ci/echecsetmoi.apk

echo ""
echo "removing unsigned apk"
rm -rf platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk

echo ""
echo "DONE"
