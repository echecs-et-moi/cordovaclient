# Echecs et moi cordova repository
Make sure you have this architecture :
```
client/
cordovaClient/
```

Make sure you have Android-Sdk install on your computer. 
If not already done set `$ANDROID_HOME` to redirect to your android-sdk repository. I.e : 

```
$ echo $ANDROID_HOME 
C:\Android\android-sdk
```

If you want to generate APK, run : 
```
$ cd cordovaClient/
$ npm run publish
```

/!\ Don't forget to change `versionCode` and `version` in `config.xml`